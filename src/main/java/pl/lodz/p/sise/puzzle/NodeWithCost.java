/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.sise.puzzle;

/**
 *
 * @author michal
 */
public class NodeWithCost extends Node implements Comparable<NodeWithCost>{
    private int currentCost;
    public NodeWithCost(Node parent,PuzzleBoard puzzleBoard,int currentCost){
        super(parent, puzzleBoard);
        this.currentCost = currentCost;
    }

    /**
     * @return the currentCost
     */
    public int getCurrentCost() {
        return currentCost;
    }

    /**
     * @param currentCost the currentCost to set
     */
    public void setCurrentCost(int currentCost) {
        this.currentCost = currentCost;
    }

    @Override
    public int compareTo(NodeWithCost o) {
        if(this.currentCost<o.currentCost)
            return -1;
        else if(this.currentCost == o.currentCost)
            return 0;
        else
            return 1;
    }
    
}
