/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.sise.puzzle;

import java.util.ArrayList;

/**
 *
 * @author michal
 */
public class Node{
    
    private Node parent;
    private PuzzleBoard puzzleBoard;
    
    public Node(Node parent,PuzzleBoard board){
        this.parent=parent;
        this.puzzleBoard=board;
    }
    

    /**
     * @return the parent
     */
    public Node getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(Node parent) {
        this.parent = parent;
    }

    /**
     * @return the puzzleBoard
     */
    public PuzzleBoard getPuzzleBoard() {
        return puzzleBoard;
    }

    /**
     * @param puzzleBoard the puzzleBoard to set
     */
    public void setPuzzleBoard(PuzzleBoard puzzleBoard) {
        this.puzzleBoard = puzzleBoard;
    }

    @Override
    public boolean equals(Object o) {
        return this.puzzleBoard.equals(((Node)o).getPuzzleBoard());
    }
    
    public String getNumbersSetting(){
        String outcome = new String("");
        for(int i=0;i<puzzleBoard.getRows();i++)
            for(int j=0;j<puzzleBoard.getColumns();j++)
                outcome+=puzzleBoard.getBoard()[i][j];
        return outcome;
    }
    
}
