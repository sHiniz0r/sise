/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.sise.puzzle;

import java.io.File;
import java.io.FileNotFoundException;

/**
 *
 * @author michal
 */
public class Main {
    public static void main(String[] args) throws FileNotFoundException{
        
        String fileName="/home/michal/Pulpit/sise";
        PuzzleBoard board = PuzzleLoader.LoadPuzzle(new File(fileName));
       
        
      
        //System.out.println(args.length);
        if(args.length>2 || args.length<=0){
            System.out.println("Za dużo parametrów lub za mało parametrów");
            return;
        }
        else{

            if(args.length == 2 && args[1].equals("-m")){
                board = board.genereateMissplacedBoard(15);
            }
                PuzzleSolver solver = new PuzzleSolver();
                String strategy = args[0];
                
                long startTime = 0;
                long elapsedTime = 0;
                
                board.printBoard();
                System.out.println();
                
                System.out.println("SOLVED");
                if(strategy.equals("--bfs")||strategy.equals("-b")){
                    System.out.println("BFS");
                    startTime = System.nanoTime();
                    solver.solveWithBfs(board);
                    elapsedTime = System.nanoTime() - startTime;
                    elapsedTime /= 1000000;
                    System.out.println(solver.createSolutionPath()+"\nCzas wykonania : "+elapsedTime + "ms");
                }
                else if(strategy.equals("-aman")){
                    System.out.println("A* Manhattan");
                    startTime = System.nanoTime();
                    solver.solveWithAStar(board, new Manhattan());
                    elapsedTime = System.nanoTime() - startTime;
                    elapsedTime /= 1000000;
                    System.out.println(solver.createSolutionPath()+"\nCzas wykonania : "+elapsedTime + "ms");
                }
                else if(strategy.equals("-amiss")){
                    System.out.println("A* Missplaced");
                    startTime = System.nanoTime();
                    solver.solveWithAStar(board, new MisplacedTiles());
                    elapsedTime = System.nanoTime() - startTime;
                    elapsedTime /= 1000000;
                    System.out.println(solver.createSolutionPath()+"\nCzas wykonania : "+elapsedTime+"ms");
                }
            
        }
        
    }
    
}
