/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.sise.puzzle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author michal
 */
public class PuzzleBoard implements Cloneable{
    
    private int[][] board;
    private int rows;
    private int columns;

    /**
     * @return the board
     */
    public int[][] getBoard() {
        return board;
    }

    /**
     * @param board the board to set
     */
    public void setBoard(int[][] board) {
        this.board = board;
    }

    /**
     * @return the rows
     */
    public int getRows() {
        return rows;
    }

    /**
     * @param rows the rows to set
     */
    public void setRows(int rows) {
        this.rows = rows;
    }

    /**
     * @return the columns
     */
    public int getColumns() {
        return columns;
    }

    /**
     * @param columns the columns to set
     */
    public void setColumns(int columns) {
        this.columns = columns;
    }
    
    @Override
    public boolean equals(Object o) {
        boolean isSame = true;
        //System.out.println(o.getClass().toString());
        PuzzleBoard board = (PuzzleBoard)o;
        for(int i=0;i<rows;i++)
            for(int j=0;j<columns;j++)
                if(this.board[i][j] != (board).getBoard()[i][j])
                    isSame=false;
        
        return isSame;
    }
    
    public enum SideToMove{
        LEFT,TOP,RIGHT,DOWN
    }
    
    public PuzzleBoard(int width,int height){
        board = new int[width][height];
        this.rows=width;
        this.columns=height;
    }
    
    public void initializeBoardWithValues(int[][] givenBoard){
        for(int i=0;i<getRows();i++)
            for(int j=0;j<getColumns();j++)
                this.getBoard()[i][j] = givenBoard[i][j];
    }
    
    public void printBoard(){
        for(int i=0;i<getRows();i++){
            System.out.println();
            for(int j=0;j<getColumns();j++){
                System.out.print(getBoard()[i][j]+"   ");
            }
        }
                
    }
    
    public String boardToString(){
        String outcome = "";
        for(int i=0;i<getRows();i++){
            outcome += "\n";
            for(int j=0;j<getColumns();j++){
                outcome += getBoard()[i][j]+"   ";
            }
        }
        return outcome;
    }
    public PuzzleBoard getSolvedPuzzleBoard(){
        int counter=1;
        int[][] temp = new int[rows][columns];
        for(int i=0;i<rows;i++)
            for(int j=0;j<columns;j++){
                temp[i][j]=counter;
                counter++;
            }
        temp[rows-1][columns-1]=0;
        
        PuzzleBoard toReturn = new PuzzleBoard(rows, columns);
        toReturn.initializeBoardWithValues(temp);
        return toReturn;
    }
    public boolean isSolved(){

        PuzzleBoard solved = this.getSolvedPuzzleBoard();
        if(Arrays.deepEquals(board, solved.getBoard()))
            return true;
        else
            return false;
                
    }
    
    public boolean checkIfCanMoveTowardsSide(PuzzleBoard.SideToMove side){
        Position zeroPosition= this.findZeroPosition();
        boolean canMove=false;
        switch(side){
            case LEFT : {
                Position leftPosition=new Position(zeroPosition.getRow(),zeroPosition.getColumn()-1);
                if(leftPosition.getRow()<0||leftPosition.getRow()>rows-1||leftPosition.getColumn()<0||leftPosition.getColumn()>columns-1)
                    canMove=false;
                else
                    canMove=true;
                break;
            }
            case RIGHT : {
                Position rightPosition=new Position(zeroPosition.getRow(),zeroPosition.getColumn()+1);
                
                if(rightPosition.getRow()<0||rightPosition.getRow()>getRows()-1||rightPosition.getColumn()<0||rightPosition.getColumn()>getColumns()-1)
                    canMove=false;
                else
                    canMove=true;
                break;
            }
            case TOP : {
                Position topPosition=new Position(zeroPosition.getRow()-1,zeroPosition.getColumn());
                if(topPosition.getRow()<0||topPosition.getRow()>getRows()-1||topPosition.getColumn()<0||topPosition.getColumn()>getColumns()-1)
                    canMove=false;
                else
                    canMove=true;
                break;
            }
            case DOWN : {
                Position downPosition=new Position(zeroPosition.getRow()+1,zeroPosition.getColumn());
                if(downPosition.getRow()<0||downPosition.getRow()>getRows()-1||downPosition.getColumn()<0||downPosition.getColumn()>getColumns()-1)
                    canMove=false;
                else
                    canMove=true;
                break;
            }
        }
        return canMove;
    }
    
    public Position findZeroPosition(){
        boolean found = false;
        Position zeroPosition=null;
        for(int i=0;i<getRows();i++){
            for(int j=0;j<getColumns();j++)
                if(getBoard()[i][j]==0){
                    zeroPosition=new Position(i, j);
                    found=true;
                    break;
                }
            if(found)
                break;
        }
        return zeroPosition;
    }
    
    public void swapPositions(Position position1,Position position2){
        int temp=0;
        temp=getBoard()[position1.getRow()][position1.getColumn()];
        getBoard()[position1.getRow()][position1.getColumn()]=getBoard()[position2.getRow()][position2.getColumn()];
        getBoard()[position2.getRow()][position2.getColumn()]=temp;
    }
    
    public ArrayList<PuzzleBoard> generatePossibleMoves(){
        ArrayList<PuzzleBoard> possibleMovesBoard = new ArrayList<PuzzleBoard>();
        Position positionToMove=null;
        if(this.checkIfCanMoveTowardsSide(SideToMove.LEFT)){
            PuzzleBoard temp = (PuzzleBoard) this.clone();
            positionToMove=new Position(temp.findZeroPosition().getRow(),temp.findZeroPosition().getColumn()-1 );
            temp.swapPositions(temp.findZeroPosition(),positionToMove );
            possibleMovesBoard.add(temp);
        }
        if(this.checkIfCanMoveTowardsSide(SideToMove.RIGHT)){
            PuzzleBoard temp = (PuzzleBoard) this.clone();
            positionToMove=new Position(temp.findZeroPosition().getRow(),temp.findZeroPosition().getColumn()+1 );
            temp.swapPositions(temp.findZeroPosition(),positionToMove );
            possibleMovesBoard.add(temp);
        }
        if(this.checkIfCanMoveTowardsSide(SideToMove.TOP)){
            PuzzleBoard temp = (PuzzleBoard) this.clone();
            positionToMove=new Position(temp.findZeroPosition().getRow()-1,temp.findZeroPosition().getColumn());
            temp.swapPositions(temp.findZeroPosition(),positionToMove );
            possibleMovesBoard.add(temp);
        }
        if(this.checkIfCanMoveTowardsSide(SideToMove.DOWN)){
            PuzzleBoard temp = (PuzzleBoard) this.clone();
            positionToMove=new Position(temp.findZeroPosition().getRow()+1,temp.findZeroPosition().getColumn() );
            temp.swapPositions(temp.findZeroPosition(),positionToMove );
            possibleMovesBoard.add(temp);
        }
        return possibleMovesBoard;
            
    }
    
    @Override
    public Object clone(){
        PuzzleBoard board = new PuzzleBoard(this.rows, this.columns);
        board.initializeBoardWithValues(this.board);
        return board;
    }
    
    public int getColumnOfSpecifiedValue(int value){
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (board[i][j] == value) {
                    return j;
                }
            }
        }
        return -1;
    }
    
    public int getRowOfSpecifiedValue(int value){
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (board[i][j] == value) {
                    return i;
                }
            }
        }
        return -1;
    }
    
    public PuzzleBoard genereateMissplacedBoard(int howManyMoves){
        PuzzleBoard puzzleBoard = this.getSolvedPuzzleBoard();
        
        Random random = new Random();
        int randomMoveIndex=0;
        ArrayList<PuzzleBoard> possibleMoves = null;
        for(int i=0;i<howManyMoves;i++){
           possibleMoves = puzzleBoard.generatePossibleMoves();
           randomMoveIndex = random.nextInt(possibleMoves.size());
           puzzleBoard = possibleMoves.get(randomMoveIndex);
            
        }
        return puzzleBoard;
            
    }
    
    
    
    
    
    
    
    
}
